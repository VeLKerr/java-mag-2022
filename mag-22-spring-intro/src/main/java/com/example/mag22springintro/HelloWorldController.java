package com.example.mag22springintro;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @RequestMapping("/hello")
    public String springHelloWorld(){
        return "Hello world!";
    }

    @RequestMapping("/sum/{id1}/{id2}")
    public String springHelloWorldWithParam(@PathVariable String id1, @PathVariable String id2){
        try {
            double d1 = Double.parseDouble(id1);
            double d2 = Double.parseDouble(id2);

            return String.format("Sum is: %f", d1 + d2);
        }
        catch (Exception e){
            throw new IllegalArgumentException("Please pass double or integer to the parameter of the calc.");
        }
    }
}
