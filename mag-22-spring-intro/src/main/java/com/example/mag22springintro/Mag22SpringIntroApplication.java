package com.example.mag22springintro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mag22SpringIntroApplication {
    public static void main(String[] args) {
        SpringApplication.run(Mag22SpringIntroApplication.class, args);
    }
}
