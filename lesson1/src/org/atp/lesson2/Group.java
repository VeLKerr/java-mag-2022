package org.atp.lesson2;

import java.util.Arrays;

public class Group {
    private static int ID = 0;
    public int id;
    private Student[] students;

    public Group() {
        this.id = ID++;
    }

    public Group(Student[] students) {
        this.id = ID++;
        this.students = students;
    }

    public void addStudent(Student student){
        if (students == null){
            students = new Student[1];
        }
        else {
            students = Arrays.copyOf(students, students.length + 1);
        }
        students[students.length - 1] = student;
    }

    public void addStudents(Student... students){
        int prevSize = 0;
        if(this.students == null){
            this.students = new Student[students.length];
        }
        else {
            prevSize = this.students.length;
            this.students = Arrays.copyOf(this.students, this.students.length + students.length);
        }
        System.arraycopy(students, 0, this.students, prevSize, students.length);
    }


    public boolean deleteStudent(Student student){
        if(students == null){
            return true;
        }
        for (int i=0; i<students.length; i++){
            if(student.equals(students[i])){
                Student[] newStudents = new Student[students.length - 1];
                System.arraycopy(students, 0, newStudents, 0, i);
                System.arraycopy(students, i+1, newStudents, i, students.length - i - 1);
                students = newStudents;
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
