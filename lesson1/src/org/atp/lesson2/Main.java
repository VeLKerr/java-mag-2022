package org.atp.lesson2;

public class Main {
    public static void main(String[] args) {
        Student studNull = new Student();
        Student student = new Student("Oleg", "Ivchenko", "o@velkerr.ru");
        System.out.println(student);

        Group gr = new Group();
        gr.addStudent(student);
        System.out.println(gr);
        gr.deleteStudent(student);
        System.out.println(gr);
        gr.addStudents(student, studNull);
        System.out.println(gr);
    }
}
