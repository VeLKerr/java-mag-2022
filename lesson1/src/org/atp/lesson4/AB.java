package org.atp.lesson4;

class A{
    String name = "Class A";
    String getName() {
        return name;
    }
}
class B extends org.atp.lesson4.arrays.A {
//    String name = "Class B"; // Никогда так не делайте (не переопределяйте поля) !!!
    String getName() {
        return name;
    }
}
public class AB{
    public static void main(String[] args) {
        org.atp.lesson4.arrays.A a = new org.atp.lesson4.arrays.A();
        org.atp.lesson4.arrays.B b = new org.atp.lesson4.arrays.B();
        org.atp.lesson4.arrays.A ab = new org.atp.lesson4.arrays.B();
        System.out.println("a : " + a.name + " " + a.getName());
        System.out.println("b : " + b.name + " " + b.getName());
        System.out.println("ab: " + ab.name + " " + ab.getName());
    }
}