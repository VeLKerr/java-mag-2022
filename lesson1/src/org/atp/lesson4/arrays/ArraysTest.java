package org.atp.lesson4.arrays;

class A{}
class B extends A{}

public class ArraysTest {
    public static void main(String[] args) {
//        Integer[] a = new Integer[]{Integer.valueOf(1)};
//        System.out.println(a.getClass());

        Object[] obj = new Object[]{};
        A[] a = new A[]{} ;
        B[] b = new B[]{};

        obj = a;
        obj = b;

        a = b;
//        b = a;
//
//        a = obj;
//        b = obj;
    }
}
