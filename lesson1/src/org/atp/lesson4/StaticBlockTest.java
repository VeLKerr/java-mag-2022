package org.atp.lesson4;

class C{
    private static int[] a = new int[5];
    static {
        for(int i=0; i<=a.length; i++){
            a[i] = i*i;
        }
        System.out.println("Init done!");
    }
}
public class StaticBlockTest {
    public static void main(String[] args) {
        System.out.println("Begin");
        try {
            C a = new C();
            C b = new C();
        }
        catch (ExceptionInInitializerError e){
            e.printStackTrace();
        }
        System.out.println("End");
        C c = new C();
    }
}
