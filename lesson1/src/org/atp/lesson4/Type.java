package org.atp.lesson4;

public enum Type {
    INT(true) {
        public Object parse(String string) { return Integer.valueOf(string); }

        public void test(){
            System.out.println("vfdvfd");
        }
    },
    INTEGER(false) {
        public Object parse(String string) { return Integer.valueOf(string); }
    },
    STRING(false) {
        public Object parse(String string) { return string; }
    };

    boolean primitive;
    Type(boolean primitive) { this.primitive = primitive; }

    public boolean isPrimitive() { return primitive; }
    public abstract Object parse(String string);
}

// 1. java.lang.Enum
// 2. enum
// 3. enum elements