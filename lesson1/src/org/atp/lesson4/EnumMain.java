package org.atp.lesson4;

public class EnumMain {
    public static void main(String[] args) {
        System.out.println(Type.class);
        System.out.println(Type.INT.getClass() + " " + Type.INT.getClass().getSuperclass());
        System.out.println(Type.INTEGER.getClass() + " " + Type.INTEGER.getClass().getSuperclass());
        System.out.println(Type.STRING.getClass()  + " " + Type.STRING.getClass().getSuperclass());
    }
}
