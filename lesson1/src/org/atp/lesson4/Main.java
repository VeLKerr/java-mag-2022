package org.atp.lesson4;

class Employee extends Object{};
class Manager extends Employee{};
class Engineer extends Employee{};

public class Main {
    public static void doSomething(Employee e) {
        if ( e instanceof Employee ) {
            System.out.println("Process an Manager");
        } else if ( e instanceof Engineer ) {
// Process an Engineer
        } else {
// Process any other type of Employee
        }
    }

    public static void main(String[] args) {
        doSomething(new Manager());
    }
}
