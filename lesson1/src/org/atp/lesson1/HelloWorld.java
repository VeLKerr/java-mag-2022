package org.atp.lesson1;

import org.w3c.dom.ls.LSOutput;

public class HelloWorld {
    public int method(){
        int loc;
        loc = 1;
        System.out.println(loc);
        StringBuilder sb = new StringBuilder("Hi ");
        sb.append("Pete ").append("Seymour");
        System.out.println(sb.toString());
        long var = 6;
        return loc;
    }

    public void method2(){
        int i = 123456789;
        float f = i;
        double d = i;
        short s = (short) i;
        System.out.println("int: " + i);
        System.out.println("float: " + f);
        System.out.println("double: " + d);
        System.out.println("short: " + s);
    }

    public void ints(){
        int a=1;
        int b=0;
        int c=a/b;
        System.out.println(c);
    }

    public void doublesInf(){
        double a=1;
        double b=0;
        double c=a/b;

        System.out.println(c);
        System.out.println("c+1 =" + (c + 1));

        System.out.println("+0.0 == -0.0 : " + (0.0 == -0.0));
        System.out.println("a/(+0.0) = " + (a/(+0.0)));
        System.out.println("a/(-0.0) = " + (a/(-0.0)));
    }

    public void doublesNan(){
        double a=0;
        double b=0;
        double c=a/b;
        System.out.println("c+0 =" + (c + 0));
        System.out.println("c<0 =" + (c < 0));
        System.out.println("c<=0 =" + (c <= 0));
        System.out.println("c>0 =" + (c > 0));
        System.out.println("c>=0 =" + (c >= 0));
        System.out.println("c==0 =" + (c == 0));
        System.out.println("c!=0 =" + (c != 0));
        System.out.println("c==c =" + (c == c)); // :)
        System.out.println("c!=c =" + (c != c)); // :)
        System.out.println("c == NaN: " + (c ==
                Double.NaN));

        System.out.println("c is NaN: " +
                Double.isNaN(c));

        int k = 0;
        if(k==0) {
            System.out.println("sfws");
        }
        int d = (k==0)? 2:3;
    }

    public static void main(String[] args) {
        System.out.println("Hello!");
        new HelloWorld().doublesNan();

        int k = 5;
        int[] a = new int[k+6];
    }
}
