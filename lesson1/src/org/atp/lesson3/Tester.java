package org.atp.lesson3;

import javax.security.auth.login.FailedLoginException;
import java.io.FileNotFoundException;

public class Tester {
    public String method() throws Exception {
        try {
            // code...
            throw new Exception("1");
        }
        catch (Exception e){
            throw new Exception("2");
        }
        finally {
            return "3";
        }
    }

    public static void main(String[] args) {
        Tester t = new Tester();
        try {
            System.out.println(t.method());
        }
        catch (FailedLoginException | FileNotFoundException e){
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

