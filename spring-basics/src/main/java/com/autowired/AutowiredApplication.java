package com.autowired;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AutowiredApplication {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(AutowiredContextConfig.class);
        ctx.refresh();

        Bean2 bean2 = ctx.getBean(Bean2.class);
        bean2.logging();

        ctx.close();
    }
}
