package com.autowired;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("345")
public class Bean2 {
    @Resource(name = "123")
    Bean1 bean1;

    public void logging() {
        System.out.println(String.format("logging from bean2 is started, and bean1: %d", bean1.hashCode()));
    }
}
