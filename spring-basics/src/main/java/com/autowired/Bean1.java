package com.autowired;

import org.springframework.stereotype.Component;

@Component("123")
public class Bean1 {
    public void logging(){
        System.out.println("logging from bean1 is started");
    }
}
