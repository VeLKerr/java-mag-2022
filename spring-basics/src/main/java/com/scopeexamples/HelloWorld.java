package com.scopeexamples;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class HelloWorld {
    private String message1;
    private String message2;
    private HelloIndia helloIndia;

    @PostConstruct
        public void init(){
        System.out.println("Hello from init method");
    }

    HelloWorld() {
        System.out.println("Inside World's constructor");
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public HelloIndia getHelloIndia() {
        return helloIndia;
    }

    public void setHelloIndia(HelloIndia helloIndia) {
        System.out.println("Helllo from India inside world");
        this.helloIndia = helloIndia;
    }


    @PreDestroy
    public void destroy(){
        System.out.println("Greetings from destroy");
    }
}
