package com.scopeexamples;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

        HelloWorld world1 = (HelloWorld) context.getBean("helloWorld");
        System.out.println(world1.getMessage1());
//        HelloWorld world2 = (HelloWorld) context.getBean("helloWorld");

        context.close();

//        HelloIndia helloIndia = (HelloIndia) context.getBean("helloIndia");
    }
}
