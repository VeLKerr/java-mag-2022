package com.profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class ProdProfileConfig implements DataSourceConfig{
    @Override
    public void setup() {
        System.out.println("Prod config");
    }
}
