package com.profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ApplicationProfile {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("dev");
        ctx.getEnvironment().setActiveProfiles("prod");

        ctx.register(DevProfileConfig.class);
        ctx.refresh();

        DevProfileConfig devProfileConfig = ctx.getBean(DevProfileConfig.class);
        devProfileConfig.setup();

        ctx.close();
    }
}
