package com.profile;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DevProfileConfig implements DataSourceConfig{
    @Value("default config")
    String dataConfig;

    @Override
    public void setup() {
        System.out.println(String.format("dev config: %s", dataConfig));
    }
}
