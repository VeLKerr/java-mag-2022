package com.annotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnotationApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(MyConfiguration.class);
        ctx.refresh();

        MyBean myBean1 = ctx.getBean(MyBean.class);
        MyBean myBean2 = ctx.getBean(MyBean.class);

        ctx.close();
    }
}
