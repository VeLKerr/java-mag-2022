package org.atp.lesson8;

public class PersonBuilder {

    private Person object = new Person();

    public Person build() {
        return object;
    }

    public PersonBuilder setName(java.lang.String value) {
        object.setName(value);
        return this;
    }

    public PersonBuilder setAveragePoints(double value) {
        object.setAveragePoints(value);
        return this;
    }

    public PersonBuilder setAddress(java.lang.String value) {
        object.setAddress(value);
        return this;
    }

    public PersonBuilder setCredit(double value) {
        object.setCredit(value);
        return this;
    }

}
