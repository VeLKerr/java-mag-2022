#### Подготовка аннотаций
Из корня проекта компилируем файлы аннотации и процессора.
```bash
javac org/atp/lesson8/annotation/BuilderProperty.java
javac org/atp/lesson8/annotation/BuilderProcessor.java 
```

#### Запуск кодогенератора
```bash
javac -classpath . -processor org.atp.lesson8.annotation.BuilderProcessor org/atp/lesson8/Person.java 
```
В classpath добавляем корень проекта (`.`) т.к. не знаем что изначально лежит в classpath.

На выходе должны увидеть скомпилированный класс `Person` и `PersonBuilder`.

#### Доп. матерималы
https://www.baeldung.com/java-annotation-processing-builder
Библиотека [Java Poet](https://www.baeldung.com/java-poet) для автоматической генерации кода. Облегчает написание методов типа `writeBuilderFile()`.
